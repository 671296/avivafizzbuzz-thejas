﻿using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using FizzBuzz.BusinessFacade.Generators;
using FizzBuzz.BusinessFacade.Models;
using FizzBuzz.Web.Controllers;
using FizzBuzz.Web.ViewModels;
using Moq;
using NUnit.Framework;

namespace FizzBuzz.Web.UnitTests
{
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        private Mock<IFizzBuzzOutputGenerator> fizzBuzzOutputGenerator = null;
        private FizzBuzzController fizzBuzzController;

        [SetUp]
        public void Setup()
        {
            this.fizzBuzzOutputGenerator = new Mock<IFizzBuzzOutputGenerator>();
            SetUpMock();
            //Arrange 
            this.fizzBuzzController = new FizzBuzzController(this.fizzBuzzOutputGenerator.Object);

        }

        private void SetUpMock()
        {
            var mockedOutput = new List<FizzBuzzModel>
                                   {
                                       new FizzBuzzModel
                                           {
                                               Description = "1"

                                           },
                                           new FizzBuzzModel
                                               {
                                                   Description = "2"
                                               },
                                               new FizzBuzzModel
                                               {
                                                   Description = "fizz"
                                               }
                                   };
            this.fizzBuzzOutputGenerator.Setup(p => p.Generate(It.IsAny<RangeModel>())).Returns(mockedOutput);
        }

        [Test]
        public void ShouldLoadIndexActionWithDefaultValue()
        {
            // Act.
            ViewResult result = fizzBuzzController.Index() as ViewResult;
            
            var inputModel = result.Model as FizzBuzzViewModel;

            // Assert.
            Assert.AreEqual(1, inputModel.Number);
        }

        [Test]
        public void ShouldLoadResultActionWithInputNumber()
        {
            var input = new FizzBuzzViewModel
            {
                Number = 3
            };
            // Act.
            ViewResult result = fizzBuzzController.GetFizzBuzzResult(input) as ViewResult;
            
            var inputModel = result.Model as FizzBuzzViewModel;

            // Assert.
            Assert.AreEqual(3, inputModel.Number);
        }

        [Test]
        public void ShouldReturnResultWithDescriptionFizzforInput3()
        {
            //Arrange
            var input = new FizzBuzzViewModel
            {
                Number = 3
            };
            // Act.
            ViewResult result = fizzBuzzController.GetFizzBuzzResult(input) as ViewResult;
            
            var inputModel = result.Model as FizzBuzzViewModel;
            var output = inputModel.FizzBuzzResult.LastOrDefault();
            // Assert.
            Assert.AreEqual("fizz", output.Description);
        }
    }
}
