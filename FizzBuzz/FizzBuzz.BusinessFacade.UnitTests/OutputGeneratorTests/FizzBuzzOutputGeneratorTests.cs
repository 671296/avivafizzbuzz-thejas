﻿using System.Collections.Generic;
using System.Linq;
using FizzBuzz.BusinessFacade.DescriptionModifiers;
using FizzBuzz.BusinessFacade.Generators;
using FizzBuzz.BusinessFacade.InputHandlers;
using FizzBuzz.BusinessFacade.Models;
using NUnit.Framework;
using Moq;

namespace FizzBuzz.BusinessFacade.UnitTests.OutputGeneratorTests
{
    [TestFixture]
    public class FizzBuzzOutputHandlerTests
    {
        private IList<IFizzBuzzInputHandler> outputHandlers = null;
        private Mock<IFizzBuzzInputHandler> divisibleByThreeHandler;
        private Mock<IFizzBuzzInputHandler> divisibleByFiveHandler;
        private Mock<IFizzBuzzInputHandler> divisibleByThreeAndFiveHandler;
        private Mock<IFizzBuzzInputHandler> defaultDivisibleHandler;
        private IFizzBuzzOutputGenerator fizzBuzzOutputGenerator = null;
        private Mock<IModifyDescription<FizzBuzzModel>> wednesdayDescriptionModifier = null;
        [SetUp]
        public void Intialize()
        {
            this.divisibleByThreeHandler = new Mock<IFizzBuzzInputHandler>();
            this.divisibleByFiveHandler = new Mock<IFizzBuzzInputHandler>();
            this.divisibleByThreeAndFiveHandler = new Mock<IFizzBuzzInputHandler>();
            this.defaultDivisibleHandler = new Mock<IFizzBuzzInputHandler>();
            this.wednesdayDescriptionModifier = new Mock<IModifyDescription<FizzBuzzModel>>();
            this.SetUpMock();
            this.outputHandlers = new List<IFizzBuzzInputHandler>
                                      {
                                          this.defaultDivisibleHandler.Object,
                                          this.divisibleByThreeHandler.Object,
                                          this.divisibleByFiveHandler.Object,
                                          this.divisibleByThreeAndFiveHandler.Object
                                      };
            var listOfModifiers = new List<IModifyDescription<FizzBuzzModel>> { this.wednesdayDescriptionModifier.Object };
            this.fizzBuzzOutputGenerator = new FizzBuzzOutputGenerator(this.outputHandlers, listOfModifiers);

        }

        private void SetUpMock()
        {
            //Mock order
            this.divisibleByThreeAndFiveHandler.Setup(p => p.Order).Returns(1);
            this.divisibleByThreeHandler.Setup(p => p.Order).Returns(2);
            this.divisibleByFiveHandler.Setup(p => p.Order).Returns(3);
            this.defaultDivisibleHandler.Setup(p => p.Order).Returns(4);

            //Mock canhandle

            this.defaultDivisibleHandler.Setup(p => p.CanHandle(It.IsAny<int>())).Returns(true);

            this.divisibleByThreeHandler.Setup(p => p.CanHandle(It.IsIn(3, 6, 9, 12))).Returns(true);

            this.divisibleByFiveHandler.Setup(p => p.CanHandle(It.IsIn(5, 10, 15, 20))).Returns(true);
            this.divisibleByThreeAndFiveHandler.Setup(p => p.CanHandle(It.IsIn(15, 30))).Returns(true);

            //Mock output
            this.defaultDivisibleHandler.Setup(p => p.Handle(It.IsAny<int>())).Returns((int input) => new FizzBuzzModel
            {
                Description = input.ToString()
            });
            this.divisibleByThreeHandler.Setup(p => p.Handle(It.IsIn(3, 6, 9, 12))).Returns((int input) => new FizzBuzzModel
            {
                Description = "fizz"
            });
            this.divisibleByFiveHandler.Setup(p => p.Handle(It.IsIn(5, 10, 15, 20))).Returns((int input) => new FizzBuzzModel
            {
                Description = "buzz"
            });
            this.divisibleByThreeAndFiveHandler.Setup(p => p.Handle(It.IsIn(15, 30))).Returns((int input) => new FizzBuzzModel
            {
                Description = "fizz buzz"
            });
            this.wednesdayDescriptionModifier.Setup(p => p.CanModify(It.IsAny<FizzBuzzModel>())).Returns(false);
            this.wednesdayDescriptionModifier.Setup(
    p => p.ModifyDescription(It.Is<FizzBuzzModel>(d => d.Description == "fizz")))
    .Callback((FizzBuzzModel input) => input.Description = "wizz");
            this.wednesdayDescriptionModifier.Setup(
                p => p.ModifyDescription(It.Is<FizzBuzzModel>(d => d.Description == "buzz")))
                .Callback((FizzBuzzModel input) => input.Description = "wuzz");
            this.wednesdayDescriptionModifier.Setup(
                 p => p.ModifyDescription(It.Is<FizzBuzzModel>(d => d.Description == "fizz buzz")))
                 .Callback((FizzBuzzModel input) => input.Description = "wizz wuzz");
        }

        [TestCase(3, "fizz")]//"Divisible by 3 test."
        [TestCase(5, "buzz")]//"Divisible by 5 test."
        [TestCase(15, "fizz buzz")]//"Divisible by 3 and 5 test."
        public void GenartedFizzBuzzResultShouldContainsExpectedResults(int endPoint, string expectedResult)
        {
            var output = this.fizzBuzzOutputGenerator.Generate(new RangeModel
            {
                StartPoint = 1,
                EndPoint = endPoint
            });

            Assert.AreEqual(expectedResult, output.LastOrDefault().Description);
        }

        [TestCase(3, "wizz")] //"Divisible by 3 test."
        [TestCase(5, "wuzz")] //"Divisible by 5 test."
        [TestCase(15, "wizz wuzz")] //"Divisible by 3 and 5 test."
        public void WhenDayOfWeekIsWednesDayThenOutputShouldChange(int endPoint, string expectedResult)
        {
            //Arrange
            this.wednesdayDescriptionModifier.Setup(p => p.CanModify(It.IsAny<FizzBuzzModel>())).Returns(true);
            var listOfModifiers = new List<IModifyDescription<FizzBuzzModel>> { this.wednesdayDescriptionModifier.Object };
            this.fizzBuzzOutputGenerator = new FizzBuzzOutputGenerator(this.outputHandlers, listOfModifiers);
            //Act
            var output = this.fizzBuzzOutputGenerator.Generate(new RangeModel
            {
                StartPoint = 1,
                EndPoint = endPoint
            });
            //Assert
            Assert.AreEqual(expectedResult, output.LastOrDefault().Description);
        }
    }
}
