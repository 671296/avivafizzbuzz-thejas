﻿using System;
using FizzBuzz.BusinessFacade.DescriptionModifiers;
using FizzBuzz.BusinessFacade.Models;
using NUnit.Framework;

namespace FizzBuzz.BusinessFacade.UnitTests.DescriptionModifiersTests
{
    [TestFixture]
    public class WednesdayDescriptionModifierTests
    {
        private IModifyDescription<FizzBuzzModel> wednesdayDescriptionModifier;

        [SetUp]
        public void Setup()
        {
            this.wednesdayDescriptionModifier = new WednesdayDescriptionModifier();
        }

        [Test]
        public void OrderOfExecutionShouldBe1()
        {
            Assert.AreEqual(1, this.wednesdayDescriptionModifier.Order);
        }

        [TestCase("fizz buzz", "wizz wuzz")]
        [TestCase("buzz", "wuzz")]
        [TestCase("fizz", "wizz")]
        public void WhenDayOfWeekIsWednesdayOutputShouldModify(string input, string expectedOutput)
        {
            var output = new FizzBuzzModel
                             {
                                 DayOfWeek = DayOfWeek.Wednesday,
                                 Description = input
                             };
            var canCondition = wednesdayDescriptionModifier.CanModify(output);
            wednesdayDescriptionModifier.ModifyDescription(output);
            Assert.AreEqual(true,canCondition);
            Assert.AreEqual(expectedOutput, output.Description);
        }
    }
}