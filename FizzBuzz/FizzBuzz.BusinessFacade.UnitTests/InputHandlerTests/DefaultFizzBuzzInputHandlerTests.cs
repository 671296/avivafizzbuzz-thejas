﻿using FizzBuzz.BusinessFacade.InputHandlers;
using NUnit.Framework;

namespace FizzBuzz.BusinessFacade.UnitTests.InputHandlerTests
{
    [TestFixture]
    public class DefaultFizzBuzzInputHandlerTests
    {
        private IFizzBuzzInputHandler defaultFizzBuzzInputHandler = null;
        [SetUp]
        public void Intialize()
        {
            this.defaultFizzBuzzInputHandler = new DefaultFizzBuzzInputHandler();//Implemented
        }

        [Test]
        public void OrderOfExecutionShouldBe4()
        {
            Assert.AreEqual(4, this.defaultFizzBuzzInputHandler.Order);
        }

        [TestCase(7)]
        [TestCase(1)]
        public void GivenAnyNumberItShouldBeAbleToHandle(int input)
        {
            var canHandle = this.defaultFizzBuzzInputHandler.CanHandle(input);

            Assert.AreEqual(true, canHandle);
        }

        [TestCase(7, "7")]
        [TestCase(1, "1")]
        public void GivenAnyNumberItShouldReturnOutputDescriptionAsInputNumber(int input, string expected)
        {
            var output = this.defaultFizzBuzzInputHandler.Handle(input);
            Assert.AreEqual(expected, output.Description);
        }
    }
}