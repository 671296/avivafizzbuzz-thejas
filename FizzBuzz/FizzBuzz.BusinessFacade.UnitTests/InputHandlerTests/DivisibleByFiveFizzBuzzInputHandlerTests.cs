﻿using FizzBuzz.BusinessFacade.InputHandlers;
using NUnit.Framework;

namespace FizzBuzz.BusinessFacade.UnitTests.InputHandlerTests
{
    [TestFixture]
    public class DivisibleByFiveFizzBuzzInputHandlerTests
    {
        private IFizzBuzzInputHandler divisibleByFiveInputHandler = null;

        [SetUp]
        public void Intialize()
        {
            this.divisibleByFiveInputHandler = new DivisibleByFiveFizzBuzzInputHandler();
        }

        [Test]
        public void OrderOfExecutionShouldBe3()
        {
            Assert.AreEqual(3, this.divisibleByFiveInputHandler.Order);
        }

        [TestCase(4, false)]
        [TestCase(5, true)]
        public void ShouldHandleNumberDivisibleOnlyBy5(int input, bool expected)
        {
            var output = this.divisibleByFiveInputHandler.CanHandle(input);
            Assert.AreEqual(expected, output);
        }

        [TestCase(10, "buzz")]
        [TestCase(5, "buzz")]
        public void ThenOutputShouldBeBuzzWhenDivisibleBy5(int input, string expectedOutput)
        {

            var output = this.divisibleByFiveInputHandler.Handle(input);

            Assert.AreEqual(expectedOutput, output.Description);
        }
    }
}