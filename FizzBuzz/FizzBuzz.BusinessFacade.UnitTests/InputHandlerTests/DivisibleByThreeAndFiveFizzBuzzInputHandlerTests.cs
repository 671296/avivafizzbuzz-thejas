﻿using FizzBuzz.BusinessFacade.InputHandlers;
using NUnit.Framework;

namespace FizzBuzz.BusinessFacade.UnitTests.InputHandlerTests
{
    [TestFixture]
    public class DivisibleByThreeAndFiveFizzBuzzInputHandlerTests
    {
        private IFizzBuzzInputHandler divisibleByThreeAndFiveInputHandler = null;

        [SetUp]
        public void Intialize()
        {
            this.divisibleByThreeAndFiveInputHandler = new DivisibleByThreeAndFiveFizzBuzzInputHandler();
        }

        [Test]
        public void OrderOfExecutionShouldBe1()
        {
            Assert.AreEqual(1, this.divisibleByThreeAndFiveInputHandler.Order);
        }

        [TestCase(4, false)]
        [TestCase(15, true)]
        public void ShouldHandleNumberOnlyDivisibleBy3And5(int input, bool expected)
        {
            var output = this.divisibleByThreeAndFiveInputHandler.CanHandle(input);
            Assert.AreEqual(expected, output);
        }

        [TestCase(30, "fizz buzz")]
        [TestCase(15, "fizz buzz")]
        public void TheOutputShouldBeFizzBuzzWhenDivisibleBy3And5(int input, string expectedOutput)
        {

            var output = this.divisibleByThreeAndFiveInputHandler.Handle(input);

            Assert.AreEqual(expectedOutput, output.Description);
        }
    }
}