﻿using FizzBuzz.BusinessFacade.InputHandlers;
using NUnit.Framework;

namespace FizzBuzz.BusinessFacade.UnitTests.InputHandlerTests
{
    [TestFixture]
    public class DivisibleByThreeFizzBuzzInputHandlerTests
    {
        private IFizzBuzzInputHandler divisibleByThreeInputHandler = null;

        [SetUp]
        public void Intialize()
        {

            this.divisibleByThreeInputHandler = new DivisibleByThreeFizzBuzzInputHandler();
        }

        [Test]
        public void OrderOfExecutionShouldBe2()
        {
            Assert.AreEqual(2, this.divisibleByThreeInputHandler.Order);
        }

        [TestCase(4, false)]
        [TestCase(3, true)]
        public void ShouldHandleNumberDivisibleBy3(int input, bool expected)
        {
            var output = this.divisibleByThreeInputHandler.CanHandle(input);
            Assert.AreEqual(expected, output);
        }

        [TestCase(6, "fizz")]
        [TestCase(3, "fizz")]
        public void DivisibleBy3ThenOutputShouldBeFizz(int input, string expectedOutput)
        {

            var output = this.divisibleByThreeInputHandler.Handle(input);

            Assert.AreEqual(expectedOutput, output.Description);
        }
    }
}