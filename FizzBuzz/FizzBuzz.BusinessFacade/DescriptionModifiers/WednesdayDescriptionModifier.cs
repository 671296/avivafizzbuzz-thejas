﻿using System;
using System.Collections.Generic;
using FizzBuzz.BusinessFacade.Models;

namespace FizzBuzz.BusinessFacade.DescriptionModifiers
{
    /// <summary>
    /// Wednesday Description Modifier.
    /// </summary>
    public class WednesdayDescriptionModifier : IModifyDescription<FizzBuzzModel>
    {
        /// <summary>
        /// Gets the order of execution.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order
        {
            get { return 1; }
        }

        /// <summary>
        /// Determines whether this instance can modify the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        ///   <c>true</c> if this instance can modify the specified input; otherwise, <c>false</c>.
        /// </returns>
        public bool CanModify(FizzBuzzModel input)
        {
            return input.DayOfWeek == DayOfWeek.Wednesday;
        }

        /// <summary>
        /// Modifies the description.
        /// </summary>
        /// <param name="input">The input.</param>
        public void ModifyDescription(FizzBuzzModel input)
        {
            //TODO:These substitution rules should be get from saperate IRepository.
            var substitutionRules = new List<FizzBuzzDescriptionSubstitutionModel>
                                       {
                                           new FizzBuzzDescriptionSubstitutionModel
                                               {
                                                   From = "fizz",
                                                   To = "wizz"
                                               },
                                           new FizzBuzzDescriptionSubstitutionModel
                                               {
                                                   From = "buzz",
                                                   To = "wuzz"
                                               }
                                       };

            foreach (var substitutionRule in substitutionRules)
            {
                input.Description = input.Description.Replace(substitutionRule.From, substitutionRule.To);
            }
        }
    }
}