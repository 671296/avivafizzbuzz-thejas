﻿namespace FizzBuzz.BusinessFacade.DescriptionModifiers
{
    /// <summary>
    /// Modifies the descriptions.
    /// </summary>
    /// <typeparam name="TInput">The type of the input.</typeparam>
    public interface IModifyDescription<TInput>
    {
        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        int Order { get; }

        /// <summary>
        /// Determines whether this instance can handle the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        ///   <c>true</c> if this instance can handle the specified input; otherwise, <c>false</c>.
        /// </returns>
        bool CanModify(TInput input);

        /// <summary>
        /// Modifies the description.
        /// </summary>
        /// <param name="input">The input.</param>
        void ModifyDescription(TInput input);
    }
}