﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzBuzz.BusinessFacade.DescriptionModifiers;
using FizzBuzz.BusinessFacade.InputHandlers;
using FizzBuzz.BusinessFacade.Models;

namespace FizzBuzz.BusinessFacade.Generators
{
    /// <summary>
    /// Generates the fizz buzz output for a given range.
    /// </summary>
    public class FizzBuzzOutputGenerator : IFizzBuzzOutputGenerator
    {
        /// <summary>
        /// The fizz buzz description modifiers.
        /// </summary>
        private readonly IEnumerable<IModifyDescription<FizzBuzzModel>> fizzBuzzDescriptionModifiers;

        /// <summary>
        /// The fizz buzz input handlers.
        /// </summary>
        private readonly IEnumerable<IFizzBuzzInputHandler> fizzBuzzInputHandlers;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzOutputGenerator" /> class.
        /// </summary>
        /// <param name="fizzBuzzInputHandlers">The fizz buzz input handlers.</param>
        /// <param name="fizzBuzzDescriptionModifiers">The description modifiers.</param>
        public FizzBuzzOutputGenerator(IEnumerable<IFizzBuzzInputHandler> fizzBuzzInputHandlers,
                                       IEnumerable<IModifyDescription<FizzBuzzModel>> fizzBuzzDescriptionModifiers)
        {
            this.fizzBuzzInputHandlers = fizzBuzzInputHandlers;
            this.fizzBuzzDescriptionModifiers = fizzBuzzDescriptionModifiers;
        }

        /// <summary>
        /// Generates fizz buzz output for specified range input.
        /// </summary>
        /// <param name="rangeInput">The range input.</param>
        /// <returns>fizz buzz output.</returns>
        public IEnumerable<FizzBuzzModel> Generate(RangeModel rangeInput)
        {
            var fizzBuzzOutputList = new List<FizzBuzzModel>();
            for (var inputNumber = rangeInput.StartPoint; inputNumber <= rangeInput.EndPoint; inputNumber++)
            {
                var handler =
                    this.fizzBuzzInputHandlers.OrderBy(o => o.Order).FirstOrDefault(p => p.CanHandle(inputNumber));
                var fizzBuzzOutput = handler.Handle(inputNumber);
                this.ModifyFizzBuzzOutput(fizzBuzzOutput);
                fizzBuzzOutputList.Add(fizzBuzzOutput);
            }
            return fizzBuzzOutputList;
        }

        /// <summary>
        /// Modifies the output.
        /// </summary>
        /// <param name="fizzBuzzOutput">The fizz buzz output.</param>
        private void ModifyFizzBuzzOutput(FizzBuzzModel fizzBuzzOutput)
        {
            //Initializing required details for modify.
            fizzBuzzOutput.DayOfWeek = DateTime.Today.DayOfWeek;
            foreach (var descriptionModifier in this.fizzBuzzDescriptionModifiers.OrderBy(p => p.Order))
            {
                if (descriptionModifier.CanModify(fizzBuzzOutput))
                {
                    descriptionModifier.ModifyDescription(fizzBuzzOutput);
                }
            }
        }
    }
}