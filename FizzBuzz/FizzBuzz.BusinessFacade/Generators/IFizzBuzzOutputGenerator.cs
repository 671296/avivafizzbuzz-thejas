﻿using System.Collections.Generic;
using FizzBuzz.BusinessFacade.Models;

namespace FizzBuzz.BusinessFacade.Generators
{
    /// <summary>
    /// Abstraction for fizzbuzz out generation.
    /// </summary>
    public interface IFizzBuzzOutputGenerator
    {
        /// <summary>
        /// Generates the specified range input.
        /// </summary>
        /// <param name="rangeInput">The range input.</param>
        /// <returns></returns>
        IEnumerable<FizzBuzzModel> Generate(RangeModel rangeInput);
    }
}