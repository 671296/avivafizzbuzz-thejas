﻿using System.Linq;
using FizzBuzz.BusinessFacade.Models;

namespace FizzBuzz.BusinessFacade.InputHandlers
{
    /// <summary>
    /// Handles all inputs which are divisible by 5.
    /// </summary>
    public class DivisibleByFiveFizzBuzzInputHandler : IFizzBuzzInputHandler
    {
        /// <summary>
        /// Gets the order execution.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order
        {
            get { return 3; }
        }

        /// <summary>
        /// Determines whether this instance can handle the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        ///   <c>true</c> if this instance can handle the specified input; otherwise, <c>false</c>.
        /// </returns>
        public bool CanHandle(int input)
        {
            return input % 5 == 0;
        }

        /// <summary>
        /// Handles the specified input number.
        /// </summary>
        /// <param name="input">The input number.</param>
        /// <returns>
        /// Fizz buzz output.
        /// </returns>
        public FizzBuzzModel Handle(int input)
        {
            return new FizzBuzzModel
            {
                Description = "buzz"
            };
        }
    }
}