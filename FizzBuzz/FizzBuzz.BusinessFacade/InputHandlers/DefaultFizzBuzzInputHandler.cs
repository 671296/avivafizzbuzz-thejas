﻿using System;
using FizzBuzz.BusinessFacade.Models;

namespace FizzBuzz.BusinessFacade.InputHandlers
{
    /// <summary>
    /// Default handler, handles all inputs and return out description as input number.
    /// </summary>
    public class DefaultFizzBuzzInputHandler : IFizzBuzzInputHandler
    {
        /// <summary>
        /// Gets the order of execution.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        public int Order
        {
            get { return 4; }
        }

        /// <summary>
        /// Determines whether this instance can handle the specified input number.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        ///   <c>true</c> if this instance can handle the specified input number; otherwise, <c>false</c>.
        /// </returns>
        public bool CanHandle(int input)
        {
            return true;
        }

        /// <summary>
        /// Handles the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// Fizz buzz output.
        /// </returns>
        public FizzBuzzModel Handle(int input)
        {
           return new FizzBuzzModel
                      {
                          Description = input.ToString()
                      };
        }
    }
}