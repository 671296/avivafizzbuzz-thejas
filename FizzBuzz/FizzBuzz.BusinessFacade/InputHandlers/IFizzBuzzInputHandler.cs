﻿using FizzBuzz.BusinessFacade.Models;

namespace FizzBuzz.BusinessFacade.InputHandlers
{
    /// <summary>
    /// Abstraction for handling fizz buzz input number.
    /// </summary>
    public interface IFizzBuzzInputHandler
    {
        /// <summary>
        /// Gets the order.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        int Order { get; }

        /// <summary>
        /// Determines whether this instance can handle the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        ///   <c>true</c> if this instance can handle the specified input; otherwise, <c>false</c>.
        /// </returns>
        bool CanHandle(int input);

        /// <summary>
        /// Handles the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>Fizz buzz output.</returns>
        FizzBuzzModel Handle(int input);
    }
}