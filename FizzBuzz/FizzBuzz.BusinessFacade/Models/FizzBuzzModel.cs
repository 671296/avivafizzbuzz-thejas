﻿using System;

namespace FizzBuzz.BusinessFacade.Models
{
    /// <summary>
    /// Fizzbuzz business model.
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the day of week.
        /// </summary>
        /// <value>
        /// The day of week.
        /// </value>
        public DayOfWeek DayOfWeek { get; set; }
    }
}