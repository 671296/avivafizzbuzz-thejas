﻿namespace FizzBuzz.BusinessFacade.Models
{
    /// <summary>
    /// FizzBuzz Description Substitution Model.
    /// </summary>
    public class FizzBuzzDescriptionSubstitutionModel
    {
        /// <summary>
        /// Gets or sets from.
        /// </summary>
        /// <value>
        /// From.
        /// </value>
        public string From { get; set; }

        /// <summary>
        /// Gets or sets to.
        /// </summary>
        /// <value>
        /// To.
        /// </value>
        public string To { get; set; }
    }
}