namespace FizzBuzz.BusinessFacade.Models
{
    /// <summary>
    /// Range model holds start and end points.
    /// </summary>
    public class RangeModel
    {
        /// <summary>
        /// Gets or sets the start point.
        /// </summary>
        /// <value>
        /// The start point.
        /// </value>
        public int StartPoint { get; set; }

        /// <summary>
        /// Gets or sets the end point.
        /// </summary>
        /// <value>
        /// The end point.
        /// </value>
        public int EndPoint { get; set; }
    }
}