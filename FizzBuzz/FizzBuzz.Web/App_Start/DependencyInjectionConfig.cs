﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using FizzBuzz.BusinessFacade.Models;

namespace FizzBuzz.Web.App_Start
{
    /// <summary>
    /// Dependency injection configuration with AutoFac.
    /// </summary>
    public static class DependencyInjectionConfig
    {
        /// <summary>
        /// Registers the dependency injection.
        /// </summary>
        public static void RegisterDependencyInjection()
        {
            var builder = new ContainerBuilder();
            var assemblies = new Assembly[] { Assembly.GetExecutingAssembly(), typeof(FizzBuzzModel).Assembly };
            builder.RegisterAssemblyTypes(assemblies).AsImplementedInterfaces();

            // Register your MVC controllers.
            builder.RegisterControllers(assemblies).InstancePerHttpRequest();
            builder.RegisterApiControllers(assemblies).InstancePerHttpRequest();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}