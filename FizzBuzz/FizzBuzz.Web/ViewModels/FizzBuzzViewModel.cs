﻿using System.ComponentModel.DataAnnotations;
using FizzBuzz.BusinessFacade.Models;
using PagedList;

namespace FizzBuzz.Web.ViewModels
{
    /// <summary>
    /// Fizz buzz view model.
    /// </summary>
    public class FizzBuzzViewModel
    {
        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        [Required(ErrorMessage = "Please enter a number.")]
        [Range(1, 1000, ErrorMessage = "Enter a number between 1 and 1000.")]
        [Display(Name = "Enter a number.")]
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the fizz buzz result.
        /// </summary>
        /// <value>
        /// The fizz buzz result.
        /// </value>
        public PagedList<FizzBuzzModel> FizzBuzzResult { get; set; }
    }
}