﻿using System.Web.Mvc;
using FizzBuzz.BusinessFacade.Generators;
using FizzBuzz.BusinessFacade.Models;
using FizzBuzz.Web.ViewModels;
using PagedList;

namespace FizzBuzz.Web.Controllers
{
    /// <summary>
    /// FizzBuzz mvc controller.
    /// </summary>
    public class FizzBuzzController : Controller
    {
        // GET: /FizzBuzz/

        /// <summary>
        /// The fizz buzz generator
        /// </summary>
        private readonly IFizzBuzzOutputGenerator fizzBuzzGenerator;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzController" /> class.
        /// </summary>
        /// <param name="fizzBuzzGenerator">The fizz buzz generator.</param>
        public FizzBuzzController(IFizzBuzzOutputGenerator fizzBuzzGenerator)
        {
            this.fizzBuzzGenerator = fizzBuzzGenerator;
        }

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var fizzBuzzViewModel = new FizzBuzzViewModel
            {
                Number = 1
            };
            return this.View(fizzBuzzViewModel);
        }

        /// <summary>
        /// Gets the fizz buzz list.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="numberOfItems">The number of items.</param>
        /// <returns></returns>
        public ActionResult GetFizzBuzzResult(FizzBuzzViewModel input, int pageNumber = 1, int numberOfItems = 20)
        {
            var fizzBuzzOutput = this.fizzBuzzGenerator.Generate(new RangeModel
            {
                StartPoint = 1,
                EndPoint = input.Number
            });

            var fizzBuzzOutputWithPaging = new PagedList<FizzBuzzModel>(fizzBuzzOutput, pageNumber, numberOfItems);

            return this.View("Index", new FizzBuzzViewModel { Number = input.Number, FizzBuzzResult = fizzBuzzOutputWithPaging });
        }
    }
}