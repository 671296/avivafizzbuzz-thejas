﻿using System;
using System.Collections.Generic;
using System.Web.Http;

namespace FizzBuzz.Web.Controllers
{
    /// <summary>
    /// REST service to capture user entered values.
    /// </summary>
    public class UserValuesController : ApiController
    {
        // GET api/<UserValues>

        /// <summary>
        /// Gets items.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> Get()
        {
            return null;//TODO:Due to time constraint unable to implement this.
        }

        // POST api/<UserValues>

        /// <summary>
        /// Posts the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        public void Post([FromBody] string value)
        {
            //Add logic to store user entered values
            new NotImplementedException();
        }
    }
}